<?php


/**
 * Created by PhpStorm.
 * User: student5
 * Date: 16.8.11
 * Time: 10.03
 */
class OrdersController

{
    public function httpGetMethod(Http $http, array $queryFields)
    {
        $ordersModel = new OrdersModel();

        $orders = $ordersModel->listAll();

        return [
            'orders'=> $orders
        ];
    }

    public function httpPostMethod(Http $http, array $formFields)
    {
        $order = new OrdersModel();

        if(isset($formFields['type']) && $formFields['type'] == 'ajax'){
            // AJAX uzklausa

            $total_items = $order->updateCart($formFields);

            $http->sendJsonResponse([
                "total_items" => $total_items
            ]);
        }
    }
}