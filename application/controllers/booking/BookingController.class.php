<?php

class BookingController

{
    public function httpGetMethod(Http $http, array $queryFields)
    {
        $bookingModel = new BookingModel();

        $booking = $bookingModel->listAll();

        return [
            'booking'=> $booking
        ];
    }

    public function httpPostMethod(Http $http, array $formFields)
    {
        if(isset($formFields['mode']) && $formFields['mode'] == 'table_edit'){

            // Rearrange multidimensional array into rows
            $row_count = count($formFields['Id']);
            $table_data = [];
            for($i = 0; $i < $row_count; $i++) {
                $data = [];
                foreach ($formFields as $key => $value) {
                    if(is_array($value)) {
                        $data[$key] = $value[$i];
                    }
                }
                $table_data[] = $data;
            }

            // Fills each row
            $booking = new BookingModel();
            foreach ($table_data as $row){
                $booking->edit($row);
            }
        }

        if(isset($formFields['mode']) && $formFields['mode'] == 'create_new') {
            $booking = new BookingModel();

            $result = $booking->create($formFields); // true arba false

            if ($result) {
                echo '<pre>';
                die('irase i DB');
            } else {
                echo '<pre>';
                die('neirase i DB');
            }
        }

        if(isset($formFields['mode']) && $formFields['mode'] == 'remove') {
            // Salinam eilute

            $bookingModel = new BookingModel();

            $bookingModel->deletebooking($formFields);
        }



        $booking = $bookingModel->listAll();

        return [
            'booking'=> $booking
        ];
    }
}