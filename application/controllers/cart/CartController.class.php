<?php

class CartController

{
    public function httpGetMethod(Http $http, array $queryFields)
    {
        $cart = $_SESSION['cart'];

        if(isset($queryFields['confirm'])){
            // Jei confirminam uzsakyma

            $order_amount = sumArrayByField($cart, 'total');

            $order = new OrdersModel();

            $order->create($cart);

            return [
              'order_amount' => $order_amount
            ];
        }else {
            //Jei atvaizduojam cart'a

            return [
                'cart' => $cart
            ];
        }
    }


    public function httpPostMethod(Http $http, array $formFields)
    {

    }
}