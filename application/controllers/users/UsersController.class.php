<?php

class UsersController
{
    public function httpGetMethod(Http $http, array $queryFields)
    {
        if(isset($_GET['logout'])){
            session_unset();
            session_destroy();
            $http->redirectTo('/');
        }
    }

    public function httpPostMethod(Http $http, array $formFields)
    {
        $user = new UsersModel();
        $error = null;

        if($formFields['type'] == 'register') {
            $user->create($formFields);
        }elseif($formFields['type'] == 'login'){
            $logged = $user->login($formFields['email'], $formFields['password']);
            if($logged){
                // prisijunges
                if(isset($_SERVER["HTTP_REFERER"])) {
                    header('Location:' . $_SERVER["HTTP_REFERER"] . '');
                } else {
                    $http->redirectTo('../index.php');
                }
            }else{
                $http->redirectTo('../index.php?error');
                // neprisijunges
//                $error = "Nepavyko prisijungti";
            }
        }elseif($formFields['type'] == 'update') {
            $user->update($formFields);
        }

        return [
            "error" => $error
        ];
    }
}