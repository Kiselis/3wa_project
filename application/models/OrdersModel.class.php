<?php

/**
 * Created by PhpStorm.
 * User: student5
 * Date: 16.8.11
 * Time: 11.27
 */
class OrdersModel
{
    public function listAll()
    {
        //database object creation
        $database = new Database();

        //query prep
        $sql = 'SELECT * FROM Meal';

        //sending querry and returning the result
        return $database->query($sql);

    }

    public function updateCart($data){
        $database = new Database();
        $found = false;

        if(isset($_SESSION['cart'])){
            foreach ($_SESSION['cart'] as $key => $item){
                if($item['id'] == $data['productId']){
                    $found = true;
                    $_SESSION['cart'][$key]['quantity'] += $data['quantity'];
                    $_SESSION['cart'][$key]['total'] = $_SESSION['cart'][$key]['quantity'] * $_SESSION['cart'][$key]['price'];
                }
            }
        }

        if(!$found) {
            $sql = "SELECT * FROM `Meal` WHERE `Id` = ?";

            $product = $database->queryOne($sql, [$data['productId']]);

            $cart_item = [
                'id' => $product['Id'],
                'name' => $product['Name'],
                'photo' => $product['Photo'],
                'quantity' => $data['quantity'],
                'price' => $product['SalePrice'],
                'total' => $data['quantity'] * $product['SalePrice']
            ];


            $_SESSION['cart'][] = $cart_item;
        }

        $total_items = sumArrayByField($_SESSION['cart'], 'quantity');

        $_SESSION['cart_total_items'] = $total_items;

        return $total_items;
    }

    public function create($data){

        // order

        $user_id = $_SESSION['user']['Id'];
        $totalAmount = sumArrayByField($data, 'total');
        $taxRate = (TAX_RATE-100)/100;
        $taxAmount = $totalAmount - ($totalAmount / TAX_RATE * 100);

        $data_to_insert = [
            $user_id,
            $totalAmount,
            $taxRate,
            $taxAmount
        ];

        $database = new Database();

        $sql = "INSERT INTO `Orders` (`User_Id`, `TotalAmount`, `TaxRate`, `TaxAmount`) VALUES (?, ?, ?, ?)";

        $new_order_id = $database->executeSql($sql, $data_to_insert);

        // orderline

        if(isset($data)) {
            foreach ($data as $key => $orderline) {

                $row = [
                    $orderline['quantity'],
                    $orderline['id'],
                    $new_order_id,
                    $orderline['price']
                ];

                $sql = "INSERT INTO `Orderline` (`QuantityOrdered`, `Meal_Id`, `Order_Id`, `PriceEach`) VALUES (?, ?, ?, ?)";

                $database->executeSql($sql, $row);
            }
        }
    }
}