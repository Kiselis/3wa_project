<?php
/**
 * Created by PhpStorm.
 * User: student5
 * Date: 16.8.12
 * Time: 10.25
 */

class BookingModel
{

    public function listAll()
    {
        $database = New DataBase();
        $sql = 'SELECT * FROM `Booking`';
        return $database->query($sql);
    }

    public function create($data)
    {

        $data['name'];
        $data['userPhone'];
        $data['date'];
        $data['hours'] = $data['hours'] < 10 ? '0' . $data['hours'] : $data['hours'];
        $data['minutes'] = $data['minutes'] < 10 ? '0' . $data['minutes'] : $data['minutes'];
        $data['bookingtime'] = $data['hours'] . ":" . $data['minutes'];
        $data['people'];

        $data_to_insert = [
            $data['name'],
            $data['userPhone'],
            $data['date'],
            $data['bookingtime'],
            intval($data['people'])
        ];

        $database = new Database();

        $sql = "INSERT INTO `Booking` (`Name`, `ContactNumber`, `BookingDate`, `BookingTime`, `NumberOfSeats`, `User_Id`) VALUES(?, ?, ?, ?, ?, " . $_SESSION['user']['Id'] . ")";

        $result = $database->executeSql($sql, $data_to_insert);

        return $result;
    }

    public function edit($data)
    {
        $database = new Database();

        $sql = "UPDATE `Booking` SET `Name` = ?, `ContactNumber` = ?, `BookingDate` = ?, `BookingTime` = ?, `NumberOfSeats` = ?, `User_Id` = ? WHERE `Id` = ?";

        $data_to_insert = [
            $data['Name'],
            $data['ContactNumber'],
            $data['BookingDate'],
            $data['BookingTime'],
            $data['NumberOfSeats'],
            $data['User_Id'],
            $data['Id']
        ];

        $result = $database->queryOne($sql, $data_to_insert);

        return $result;
    }

    public function deletebooking($data)
    {
        $database = new Database();
        $sql = "DELETE FROM `Booking` WHERE `Id` = ?";

        $row = $database->queryOne($sql, [$data['bookingId']]);

        return $row;
    }
}