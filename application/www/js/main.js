'use strict';

/////////////////////////////////////////////////////////////////////////////////////////
// FONCTIONS                                                                           //
/////////////////////////////////////////////////////////////////////////////////////////




/////////////////////////////////////////////////////////////////////////////////////////
// CODE PRINCIPAL                                                                      //
/////////////////////////////////////////////////////////////////////////////////////////

$(document).ready(function(){

   $(".addToCart").click(function(e){
       e.preventDefault();

       var productId = $(this).data('product-id');
       var quantity = $(this).parent().parent().find('input[type=number]').val();


       $.ajax({
           method: 'POST',
           url: 'orders',
           data: {
               productId: productId,
               quantity: quantity,
               type: 'ajax'
           },
           dataType: 'json',
           success: function(response){
               $('#total-items').html(response.total_items);
           }
       })
   });

    $(".deletebooking").click(function(){
        var btn = $(this);
        var bookingId = $(this).data('booking-id');
        $.ajax({
            method: 'POST',
            url: 'booking',
            data:{
                bookingId: bookingId,
                mode: 'remove'
            },
            success: function(){

                btn.parent().parent().remove();
            }
        })


    });
});