<?php

const TAX_RATE = 121;

function sumArrayByField ($array, $key) {
    $sum = 0;
    foreach ($array as $item) {
        if(isset($item[$key])){
            $sum += $item[$key];
        }
    }
    return $sum;
}

function dd($data){
    echo "<pre>";
    var_dump($data);
    echo "</pre>";
    die();
}